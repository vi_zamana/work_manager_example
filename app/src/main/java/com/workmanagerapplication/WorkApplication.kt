package com.workmanagerapplication

import android.app.Application
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.workmanagerapplication.db.AppDatabase
import com.workmanagerapplication.di.AppModule
import com.workmanagerapplication.di.ApplicationComponent
import com.workmanagerapplication.di.DaggerApplicationComponent
import com.workmanagerapplication.workers.SeedDatabaseWorker

class WorkApplication : Application() {

    companion object {

        private const val DATABASE_NAME = "plantDatabase"

        lateinit var appComponent: ApplicationComponent
    }

    lateinit var database: AppDatabase

    override fun onCreate() {
        super.onCreate()
        initAppComponent()
        initDB()
    }

    private fun initAppComponent() {
        appComponent = DaggerApplicationComponent.builder()
            .appModule(AppModule(this))
            .build()
    }

    private fun initDB() {
        database = Room.databaseBuilder(this, AppDatabase::class.java, DATABASE_NAME)
            .allowMainThreadQueries()
            .fallbackToDestructiveMigration()
            .addCallback(object : RoomDatabase.Callback() {
                override fun onCreate(db: SupportSQLiteDatabase) {
                    super.onCreate(db)
                    val request = OneTimeWorkRequest.Builder(SeedDatabaseWorker::class.java).build()
                    WorkManager.getInstance().enqueue(request)
                }
            })
            .build()
    }
}