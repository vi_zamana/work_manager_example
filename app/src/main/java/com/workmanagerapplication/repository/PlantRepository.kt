package com.workmanagerapplication.repository

import com.workmanagerapplication.db.AppDatabase
import javax.inject.Inject

class PlantRepository @Inject constructor(private val database: AppDatabase) {

    fun getList() = database.plantDao().getList()
}