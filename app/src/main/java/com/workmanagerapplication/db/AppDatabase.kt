package com.workmanagerapplication.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.workmanagerapplication.model.Plant
import com.workmanagerapplication.workers.SeedDatabaseWorker

@Database(entities = [Plant::class], version = 2)
abstract class AppDatabase : RoomDatabase() {

    abstract fun plantDao(): PlantDao
}