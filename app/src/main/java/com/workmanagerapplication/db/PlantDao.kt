package com.workmanagerapplication.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.workmanagerapplication.model.Plant
import io.reactivex.Flowable

@Dao
interface PlantDao {

    @Query("SELECT * FROM plants")
    fun getList(): Flowable<List<Plant>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(plants: List<Plant>)
}