package com.workmanagerapplication.workers

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import com.workmanagerapplication.WorkApplication
import com.workmanagerapplication.db.AppDatabase
import com.workmanagerapplication.model.Plant
import javax.inject.Inject

class SeedDatabaseWorker(
    context: Context,
    workerParams: WorkerParameters
) : Worker(context, workerParams) {

    companion object {
        private const val FILE_NAME = "plants.json"
    }

    @Inject
    lateinit var database: AppDatabase

    override fun doWork(): Result {
        WorkApplication.appComponent.inject(this)
        return try {
            applicationContext.assets.open(FILE_NAME).use { inputStream ->
                JsonReader(inputStream.reader()).use { jsonReader ->
                    val plantType = object : TypeToken<List<Plant>>() {}.type
                    val plantList: List<Plant> = Gson().fromJson(jsonReader, plantType)

                    database.plantDao().insertAll(plantList)

                    Result.success()
                }
            }
        } catch (ex: Exception) {
            Result.failure()
        }
    }
}