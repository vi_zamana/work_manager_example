package com.workmanagerapplication.ui.plant

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.workmanagerapplication.R
import com.workmanagerapplication.WorkApplication
import com.workmanagerapplication.model.Plant
import com.workmanagerapplication.ui.plant.adapter.PlantAdapter
import kotlinx.android.synthetic.main.fragment_plant.*
import javax.inject.Inject

class PlantFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: PlantViewModelFactory

    private lateinit var viewModel: PlantViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_plant, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WorkApplication.appComponent.inject(this)
        viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(PlantViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
       viewModel.list.observe(this, Observer<ArrayList<Plant>> {
            it?.let(::initAdapter)
        })
        viewModel.loadPlants()
    }

    private fun initAdapter(list: ArrayList<Plant>) {
        recyclerView.run {
            adapter = PlantAdapter()
            layoutManager = LinearLayoutManager(context)
            (adapter as PlantAdapter).setItems(list)
        }
    }
}