package com.workmanagerapplication.ui.plant.adapter

import androidx.recyclerview.widget.RecyclerView
import com.workmanagerapplication.databinding.ItemPlantBinding
import com.workmanagerapplication.model.Plant

class PlantViewHolder (private val itemBinding: ItemPlantBinding)
    : RecyclerView.ViewHolder(itemBinding.root) {

    fun bind(plant: Plant) {
        itemBinding.item = plant
    }
}