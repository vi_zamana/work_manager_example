package com.workmanagerapplication.ui.plant

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.workmanagerapplication.model.Plant
import com.workmanagerapplication.repository.PlantRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class PlantViewModel(private val repository: PlantRepository) : ViewModel() {

    private val disposables = CompositeDisposable()

    val list = MutableLiveData<ArrayList<Plant>>()

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

    fun loadPlants() {
        repository.getList()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                list.value = ArrayList<Plant>().apply { addAll(it) }
            }, {})
            .let(disposables::add)
    }
}