package com.workmanagerapplication.ui.plant

import com.workmanagerapplication.repository.PlantRepository
import com.workmanagerapplication.ui.base.BaseViewModelFactory
import javax.inject.Inject

class PlantViewModelFactory @Inject constructor(private val repository: PlantRepository) :
    BaseViewModelFactory<PlantViewModel>(PlantViewModel::class.java) {

    override fun createViewModel() = PlantViewModel(repository)
}