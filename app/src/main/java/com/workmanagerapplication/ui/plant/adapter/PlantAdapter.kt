package com.workmanagerapplication.ui.plant.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.workmanagerapplication.R
import com.workmanagerapplication.databinding.ItemPlantBinding
import com.workmanagerapplication.model.Plant

class PlantAdapter : RecyclerView.Adapter<PlantViewHolder>() {

    private val itemList: ArrayList<Plant> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlantViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemBinding = DataBindingUtil.inflate(inflater, R.layout.item_plant,
            parent, false) as ItemPlantBinding
        return PlantViewHolder(itemBinding)
    }

    override fun getItemCount() = itemList.size

    override fun onBindViewHolder(holder: PlantViewHolder, position: Int) {
        holder.bind(itemList[position])
    }

    fun setItems(list: List<Plant>) {
        itemList.addAll(list)
        notifyDataSetChanged()
    }
}