package com.workmanagerapplication.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.workmanagerapplication.R
import com.workmanagerapplication.ui.plant.PlantFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction().replace(R.id.container, PlantFragment()).commit()
    }
}
