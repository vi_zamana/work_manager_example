package com.workmanagerapplication.di

import com.workmanagerapplication.ui.plant.PlantFragment
import com.workmanagerapplication.workers.SeedDatabaseWorker
import dagger.Component
import javax.inject.Singleton

@Component(modules = [AppModule::class])
@Singleton
interface ApplicationComponent {

    fun inject(fragment: PlantFragment)

    fun inject(worker: SeedDatabaseWorker)
}