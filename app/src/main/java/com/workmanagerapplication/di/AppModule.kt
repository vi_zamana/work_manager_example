package com.workmanagerapplication.di

import android.content.Context
import com.workmanagerapplication.WorkApplication
import com.workmanagerapplication.db.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val app: WorkApplication) {

    @Provides
    @Singleton
    fun provideApplication(): WorkApplication = app

    @Provides
    @Singleton
    fun provideApplicationContext(app: WorkApplication): Context = app

    @Provides
    @Singleton
    fun provideDao(app: WorkApplication): AppDatabase = app.database
}